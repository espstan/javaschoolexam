package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;

public class Calculator {
    private static int k = 0;

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        if (statement == null)
            return null;
        int size = statement.length();
        if (size == 0)
            return null;

        Stack<Double> stackNumbers = new Stack<>();
        Stack<String> stackSymbols = new Stack<>();
        String numbers = "1234567890";
        String lastSymbol = statement.substring(size - 1, size);
        String lastParenthesesot = "";

        if (!numbers.contains(lastSymbol) && !lastSymbol.equals(")"))
            return null;
        if (statement.contains(",") || statement.contains(" ") || statement.contains(".."))
            return null;

        for (int i = 0; i < size; i++) {
            if (isNumber(statement, i)) {
                double currentNumber = getNumber(statement, i);
                i = k;
                stackNumbers.push(currentNumber);
            } else {
                if (isSymbol(statement, i)) {
                    String str = getSymbol(statement, i);
                    if (i + 1 < statement.length()) {
                        if (isSymbol(statement, i + 1))
                            return null;
                    }
                    if (stackSymbols.size() != 0) {
                        int priority1 = getPriority(stackSymbols.lastElement());
                        int priority2 = getPriority(str);

                        if (priority1 < priority2) {
                            stackSymbols.push(str);
                        } else {
                            if (getOperation(stackSymbols.lastElement(), stackNumbers, stackSymbols)) {
                                stackSymbols.push(str);
                            } else
                                return null;
                        }
                    } else {
                        stackSymbols.push(str);
                    }
                } else {
                    if (isParenthesesot(statement, i)) {
                        String str = getSymbol(statement, i);

                        if (str.equals("(")) {
                            stackSymbols.push(str);
                            lastParenthesesot = str;
                        }

                        if (str.equals(")")) {
                            if (!lastParenthesesot.equals("(")) {
                                return null;
                            } else {
                                while (!stackSymbols.lastElement().equals(lastParenthesesot)) {
                                    if (!getOperation(stackSymbols.lastElement(), stackNumbers, stackSymbols))
                                       return null;
                                }
                                stackSymbols.pop();
                                if (stackSymbols.contains("("))
                                    lastParenthesesot = "(";
                                else
                                    lastParenthesesot = "";
                            }
                        }
                    }
                }
            }
        }
        while (stackSymbols.size() != 0) {
            if (getOperation(stackSymbols.lastElement(), stackNumbers, stackSymbols))
                continue;
            else
                return null;
        }
        double finalResult = stackNumbers.pop();
        if (finalResult == (int) finalResult) {
            return String.valueOf((int) finalResult);
        }
        return Double.toString((double) (int) Math.round(finalResult*10000)/10000);
    }

    private boolean getOperation(String str, Stack<Double> numbers, Stack<String> symbols) {
        if (numbers.size() < 2) return false;
        double d1;
        double d2;
        switch (str) {
            case "+":
                d1 = numbers.pop();
                d2 = numbers.pop();
                numbers.push(d1 + d2);
                symbols.pop();
                break;
            case "-":
                d1 = numbers.pop();
                d2 = numbers.pop();
                numbers.push(d2 - d1);
                symbols.pop();
                break;
            case "*":
                d1 = numbers.pop();
                d2 = numbers.pop();
                numbers.push(d1 * d2);
                symbols.pop();
                break;
            case "/":
                d1 = numbers.pop();
                d2 = numbers.pop();
                if (d1 == 0)
                    return false;
                else {
                    numbers.push(d2 / d1);
                    symbols.pop();
                }
                break;
        }
        return true;
    }

    private double getNumber(String str, int pos) {
        k = pos;
        int size = str.length();
        StringBuilder sb = new StringBuilder();
        sb.append(str.substring(pos, pos + 1));
        pos++;
        boolean longNumber = false;
        while (pos < size) {
            if (isNumber(str, pos)) {
                sb.append(str.substring(pos, pos + 1));
                longNumber = true;
                pos++;
            } else {
                break;
            }
        }
        if (longNumber)
            k = pos - 1;
        return Double.valueOf(sb.toString());
    }

    private String getSymbol(String str, int pos) {


        return str.substring(pos, pos + 1);
    }

    private boolean isSymbol(String str, int pos) {
        String symbols = "/*+-";
        String current = str.substring(pos, pos + 1);
        if (symbols.contains(current)) {
            return true;
        } else
            return false;
    }

    private boolean isNumber(String str, int pos) {
        String numbers = "0123456789.";
        String current = str.substring(pos, pos + 1);
        if (numbers.contains(current)) {
            return true;
        } else
            return false;
    }

    private boolean isParenthesesot(String str, int pos) {
        String parenthesesots = "()";
        String current = str.substring(pos, pos + 1);
        if (parenthesesots.contains(current)) {
            return true;
        } else
            return false;
    }

    private int getPriority(String str) {
        switch (str) {
            case "*":
            case "/":
                return 2;
            case "+":
            case "-":
                return 1;
        }
        return 0;
    }
}
