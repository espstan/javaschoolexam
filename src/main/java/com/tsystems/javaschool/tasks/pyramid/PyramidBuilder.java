package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {
    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        int count = getPyramidHeight(inputNumbers);
        int width = 2 * count - 1;
        int[][] pyramid = new int[count][width];
        Collections.sort(inputNumbers);
        int middle = width / 2;
        int position = 0;
        for (int i = 0; i < count; i++) {
            int sum = 0;
            middle = middle - i;
            for (int j = 0; j < width; j++) {
                if (j != middle) {
                    pyramid[i][j] = 0;
                } else {
                    pyramid[i][j] = inputNumbers.get(position);
                    position++;
                    sum++;
                    if (sum != i + 1)
                        middle += 2;
                }
            }
            middle = width / 2;
        }
        for (int[] ints : pyramid) {
            for (int anInt : ints) {
                System.out.print(anInt + " ");
            }
            System.out.println();
        }
        return pyramid;
    }

    private int getPyramidHeight(List<Integer> inputNumbers) {
        int size = inputNumbers.size();
        if (inputNumbers.contains(null))
            throw new CannotBuildPyramidException();
        int count = 1;
        while (true) {
            size -= count;
            if (size == 0)
                break;
            else {
                if (size < 0) throw new CannotBuildPyramidException();
                count++;
            }
        }
        return count;
    }
}